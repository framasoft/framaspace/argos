# Changelog

## [Unreleased]

## 0.9.0

Date: 2025-02-18

- 🐛 — Fix worker timeout for old results cleaning in recurring tasks (#84)

💥 Old results are now removed by their age, not based on their number.

💥 Warning: `max_results` setting has been replaced by `max_results_age`, which is a duration.
Use `argos server generate-config > /etc/argos/config.yaml-dist` to generate
a new example configuration file.

## 0.8.2

Date: 2025-02-18

- 🐛 — Fix recurring tasks with gunicorn

## 0.8.1

Date: 2025-02-18

- 🐛 — Fix todo enum in jobs table

## 0.8.0

Date: 2025-02-18

- ✨ — Allow to customize agent User-Agent header (#78)
- 📝 — Document how to add data to requests (#77)
- ✨ — No need cron tasks for DB cleaning anymore (#74 and #75)
- ✨ — No need cron tasks for agents watching (#76)
- ✨ — Reload configuration asynchronously (#79)
- 🐛 — Automatically reconnect to LDAP if unreachable (#81)
- 🐛 — Better httpx.RequestError handling (#83)

💥 Warning: there is new settings to add to your configuration file.
Use `argos server generate-config > /etc/argos/config.yaml-dist` to generate
a new example configuration file.

💥 You don’t need cron tasks anymore!
Remove your old cron tasks as they will now do nothing but generating errors.

NB: You may want to add `--enqueue` to `reload-config` command in your systemd file.

## 0.7.4

Date: 2025-02-12

- 🐛 — Fix method enum in tasks table (thx to Dryusdan)

## 0.7.3

Date: 2025-01-26

- 🐛 — Fix bug in retry_before_notification logic when success

## 0.7.2

Date: 2025-01-24

- 🐛 — Fix bug in retry_before_notification logic

## 0.7.1

Date: 2025-01-15

- 🩹 — Avoid warning from MySQL only alembic instructions
- 🩹 — Check before adding/removing ip_version_enum
- 📝 — Improve release documentation


## 0.7.0

Date: 2025-01-14

- ✨ — IPv4/IPv6 choice for checks, and choice for a dual-stack check (#69)
- ⚡ — Mutualize check requests (#68)
- ✨ — Ability to delay notification after X failures (#71)
- 🐛 — Fix bug when changing IP version not removing tasks (#72)
- ✨ — Allow to specify form data and headers for checks (#70)
- 🚸 — Add a long expiration date on auto-refresh cookies
- 🗃️ — Use bigint type for results id column in PostgreSQL (#73)

## 0.6.1

Date: 2024-11-28

- 🐛 - Fix database migrations without default values
- 🐛 - Fix domain status selector’s bug on page refresh

## 0.6.0

Date: 2024-11-28

- 💄 — Show only not-OK domains by default in domains list, to reduce the load on browser
- ♿️ — Fix not-OK domains display if javascript is disabled
- ✨ — Retry check right after a httpx.ReadError
- ✨ — The HTTP method used by checks is now configurable
- ♻️ — Refactor some agent code
- 💄 — Filter form on domains list (#66)
- ✨ — Add "Remember me" checkbox on login (#65)
- ✨ — Add a setting to set a reschedule delay if check failed (#67)  
  BREAKING CHANGE: `mo` is no longer accepted for declaring a duration in month in the configuration  
  You need to use `M`, `month` or `months`
- ✨ - Allow to choose a frequency smaller than a minute
- ✨🛂 — Allow partial or total anonymous access to web interface (#63)
- ✨🛂 — Allow to use a LDAP server for authentication (#64)

## 0.5.0

Date: 2024-09-26

- 💄 — Correctly show results on small screens
- 📝💄 — Add opengraph tags to documentation site (#62)
- 🔨 — Add a small web server to browse documentation when developing
- ✨ — Add new check type: http-to-https (#61)
- 👷 — Remove Unreleased section from CHANGELOG when publishing documentation
- 🩹 — Severity of ssl-certificate-expiration’s errors is now UNKNOWN (#60)
- 💄 — Better display of results’ error details

## 0.4.1

Date: 2024-09-18

- 💄 — Use a custom User-Agent header
- 🐛 — Fix mail and gotify alerting

## 0.4.0

Date: 2024-09-04

- 💄 — Improve email and gotify notifications
- ✨ — Add command to test gotify configuration
- ✨ — Add nagios command to use as a Nagios probe
- ✨ — Add Apprise as notification way (#50)

## 0.3.1

Date: 2024-09-02

- ✨ — Add new check types: body-like, headers-like and json-like (#58)

## 0.3.0

Date: 2024-09-02

- 🩹 — Fix release documentation
- ✅ — Add mypy test
- ✨ — Add new check type: status-in
- 🩹 — Close menu after rescheduling non-ok checks (#55)
- ✨ — Add new check types: headers-contain and headers-have (#56)
- ✨ — Add command to test email configuration (!66)
- 💄 — Enhance the mobile view (!67)
- ✨ — Allow to run Argos in a subfolder (i.e. not on /) (#59)
- ✨ — Add new check types: json-contains, json-has and json-is (#57)

## 0.2.2

Date: 2024-07-04

- 🐛 — Fix bug in login view when having an expired token in cookie (redirect loop)

## 0.2.1

Date: 2024-06-27

- 🐛 — Fix bug in login view when having a cookie (internal server error)

## 0.2.0

Date: 2024-06-24

- 💄📯 — Improve notifications and result(s) pages
- 🔊 — Add level of log before the log message
- 🔊 — Add a warning message in the logs if there is no tasks in database. (fix #41)
- ✨ — Add command to generate example configuration (fix #38)
- 📝 — Improve documentation
- ✨ — Add command to warn if it’s been long since last viewing an agent (fix #49)
- 💥 — Change default config file path to argos-config.yaml (fix #36)
- 📝 — New documentation URL: doc is now on https://argos-monitoring.framasoft.org/
- 💥 — Remove env vars and only use the configuration file
- ✨ — Add built-in authentication for human interface

## 0.1.1

Date: 2024-04-30

- 🐛 — Fix --config option for some commands
- ☝️🧐 — Validate config file readability

## 0.1.0

Date: 2024-04-25

- First working version
