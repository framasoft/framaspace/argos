import re
import sys


def fix_output(matchobj):
    return f"{matchobj.group(1)}{float(matchobj.group(2)) * 10}/{int(matchobj.group(3)) * 10}"


pattern = re.compile(r"(Your code has been rated at )([0-9.]+)/(10)")
for line in sys.stdin:
    line.rstrip()
    print(re.sub(pattern, fix_output, line), end="")
