from typing import Literal


IPVersion = Literal["4", "6"]

Method = Literal[
    "GET", "HEAD", "POST", "OPTIONS", "CONNECT", "TRACE", "PUT", "PATCH", "DELETE"
]

Todo = Literal["RELOAD_CONFIG"]
