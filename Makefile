.DEFAULT_GOAL := help
RED=\033[0;31m
GREEN=\033[0;32m
ORANGE=\033[0;33m
BLUE=\033[0;34m
NC=\033[0m # No Color

.PHONY: test lint djlint pylint ruff mypy

venv: ## Create the venv
	python3 -m venv venv
develop: venv ## Install the dev dependencies
	venv/bin/pip install -e ".[dev,docs,ldap]"
docs: cog ## Build the docs
	venv/bin/sphinx-build docs public
	if [ ! -e "public/mermaid.min.js" ]; then curl -sL $$(grep mermaid.min.js public/search.html | cut -f 2 -d '"') --output public/mermaid.min.js; fi
	sed -e 's@https://unpkg.com/mermaid[^"]*"@mermaid.min.js"@' -i public/search.html public/genindex.html
	sed -e 's@https://unpkg.com/mermaid[^"]*"@../mermaid.min.js"@' -i public/developer/models.html public/developer/overview.html
docs-webserver: docs
	python3 -m http.server -d public -b 127.0.0.1 8001
cog: ## Run cog, to integrate the CLI options to the docs.
	venv/bin/cog -r docs/*.md
test: venv ## Run the tests
	venv/bin/pytest
ruff: venv
	venv/bin/ruff format --check .
ruff-format: venv
	venv/bin/ruff format .
djlint: venv ## Format the templates
	venv/bin/djlint --ignore=H006 --profile jinja --lint argos/server/templates/*html
pylint: venv ## Runs pylint on the code
	venv/bin/pylint argos
mypy: venv
	venv/bin/mypy argos tests
lint: djlint pylint mypy ruff
help:
	@python3 -c "$$PRINT_HELP_PYSCRIPT" < $(MAKEFILE_LIST)

# See https://daniel.feldroy.com/posts/autodocumenting-makefiles
define PRINT_HELP_PYSCRIPT # start of Python section
import re, sys

output = []
# Loop through the lines in this file
for line in sys.stdin:
    # if the line has a command and a comment start with
    #   two pound signs, add it to the output
    match = re.match(r'^([a-zA-Z_-]+):.*?## (.*)$$', line)
    if match:
        target, help = match.groups()
        output.append("\033[36m%-25s\033[0m %s" % (target, help))
# Sort the output in alphanumeric order
output.sort()
# Print the help result
print('\n'.join(output))
endef
export PRINT_HELP_PYSCRIPT # End of python section
