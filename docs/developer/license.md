---
description: Argos is licensed under the terms of the GNU AFFERO GPLv3.
---
# License

Argos is licensed under the terms of the GNU AFFERO GPLv3.

See [LICENSE file](https://framagit.org/framasoft/framaspace/argos/-/blob/main/LICENSE) on the repository.
