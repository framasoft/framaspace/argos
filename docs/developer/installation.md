---
description: All you need to know to develop on Argos.
---
# Installing for development

To install all what you need to develop on Argos, do:

```bash
git clone https://framagit.org/framasoft/framaspace/argos.git
cd argos
make develop
```

It will create a virtualenv and install all the needed dependencies in it.
